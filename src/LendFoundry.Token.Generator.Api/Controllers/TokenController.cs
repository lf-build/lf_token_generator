﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Token.Generator.Api
{
    /// <summary>
    /// Represents the Token controller class.
    /// </summary>
    [Route("/")]
    public class TokenController : ExtendedController
    {
        /// <summary>
        /// Defined private token generator service property.
        /// </summary>
        private ITokenGeneratorService TokenGeneratorService { get; }

        /// <summary>
        /// Token controller construction
        /// </summary>
        /// <param name="tokenGeneratorService">Token generator service object</param>
        /// <param name="logger">Logger object</param>
        public TokenController(ITokenGeneratorService tokenGeneratorService,ILogger logger):base(logger)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));

            TokenGeneratorService = tokenGeneratorService;
        }

        /// <summary>
        /// Issue the token
        /// </summary>
        /// <param name="request"> Token generate request object</param>
        /// <returns>Token generate response object</returns>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(ITokenGenerate))]       
        public async Task<IActionResult> IssueToken([FromBody] TokenGenerate request)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await TokenGeneratorService.Issue(request)));
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, $"Invalid {exception.ParamName}.");
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, $"Invalid {exception.ParamName}.");
            }
           
        }
        /// <summary>
        /// Validate the token
        /// </summary>
        /// <param name="validateTokenRequest"> Validate token request object</param>
        /// <returns>return true or false</returns>
        [HttpPost("/validate")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(bool))]
        public async Task<IActionResult> ValidateToken([FromBody] ValidateTokenRequest validateTokenRequest)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await TokenGeneratorService.ValidateToken(validateTokenRequest)));
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, $"Invalid {exception.ParamName}.");
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, $"Invalid {exception.ParamName}.");
            }

        }

    }
}
