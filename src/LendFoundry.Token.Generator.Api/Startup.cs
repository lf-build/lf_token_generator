﻿using System;
using System.Runtime;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Token.Generator.Configuration;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

namespace LendFoundry.Token.Generator.Api
{
    /// <summary>
    /// Startup service
    /// </summary>
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Token Generator"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Token.Generator.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddHttpServiceLogging(LendFoundry.Token.Generator.Settings.ServiceName);            
            services.AddTransient<ITokenGeneratorService, TokenGeneratorService>();
            services.AddTransient<ITokenConfig, TokenConfig>();
            services.AddTransient<ITokenHandler, TokenHandler>();            
            services.AddConfigurationService<TokenGeneratorConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<TokenGeneratorConfiguration>(Settings.ServiceName);
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<TokenGeneratorConfiguration>>().Get());
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
            Console.WriteLine(GCSettings.LatencyMode);
            Console.WriteLine(GCSettings.IsServerGC);
            Console.WriteLine(GCSettings.LargeObjectHeapCompactionMode);
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Token Generator");
            });
            #else
            app.UseSwaggerDocumentation();
            #endif

            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}