using System;


namespace LendFoundry.Token.Generator
{
    public class ValidateTokenRequest : IValidateTokenRequest
    {
        
      public  string EntityId { get; set; }

      public  string EntityType { get; set; }

      public  string Token { get; set; }
    }
}