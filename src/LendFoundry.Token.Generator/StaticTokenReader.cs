using LendFoundry.Security.Tokens;

namespace LendFoundry.Token.Generator
{
    public class StaticTokenReader : ITokenReader
    {
        public StaticTokenReader(string token)
        {
            Token = token;
        }

        private string Token { get; }

        public string Read()
        {
            return Token;
        }
    }
}