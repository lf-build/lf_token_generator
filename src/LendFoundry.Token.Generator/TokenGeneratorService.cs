using System;
using Jose;
using LendFoundry.Security.Tokens;
using System.Threading.Tasks;
using LendFoundry.Token.Generator.Configuration;

namespace LendFoundry.Token.Generator
{
    public class TokenGeneratorService : ITokenGeneratorService
    {
        public TokenGeneratorService(ITokenConfig config, ITokenReader reader, TokenGeneratorConfiguration tokenGeneratorConfiguration)
        {
            Reader = reader;
#if DOTNET2
            JWT.DefaultSettings.JsonMapper = new JsonNetMapper();
#else
            JWT.JsonMapper = new JsonNetMapper();
#endif
            Configuration = config ?? new TokenConfig();
            TokenGeneratorConfiguration = tokenGeneratorConfiguration;

        }

        private ITokenReader Reader { get; }
     
        private TokenGeneratorConfiguration TokenGeneratorConfiguration { get; set; }

        private ITokenConfig Configuration { get; }

        public async Task<ITokenGenerate> Issue(ITokenGenerate tokenRequest)
        {
            if (string.IsNullOrWhiteSpace(tokenRequest.EntityType))
                throw new ArgumentException("EntityType is required.");

            if (string.IsNullOrWhiteSpace(tokenRequest.EntityId))
                throw new ArgumentException("EntityId is required.");

            if (string.IsNullOrWhiteSpace(tokenRequest.Tenant))
                throw new ArgumentException("Tenant is required.");


            await Task.Yield();
            var token = new TokenGenerate
            {
                Tenant = tokenRequest.Tenant,
                Issuer = tokenRequest.Issuer,
                IssuedAt = DateTime.UtcNow,
                Expiration = tokenRequest.ExpirationDays != 0 ? DateTime.UtcNow.AddDays(tokenRequest.ExpirationDays) : DateTime.UtcNow.AddDays(TokenGeneratorConfiguration.ExpirationDays),
                ExpirationDays = tokenRequest.ExpirationDays,
                Subject = tokenRequest.Subject,
                EntityId = tokenRequest.EntityId,
                EntityType = tokenRequest.EntityType

            };

            token.Value = JWT.Encode(token, Configuration.SecretKey, JwsAlgorithm.HS256);

            return token;
        }



        public async Task<bool> ValidateToken(IValidateTokenRequest validateTokenRequest)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(validateTokenRequest.EntityType))
                    throw new ArgumentException("EntityType is required.");

                if (string.IsNullOrWhiteSpace(validateTokenRequest.EntityId))
                    throw new ArgumentException("EntityId is required.");

                if (string.IsNullOrWhiteSpace(validateTokenRequest.Token))
                    throw new ArgumentException("Token is required.");
                await Task.Yield();
                validateTokenRequest.Token = validateTokenRequest.Token ?? Reader.Read();
                var token = JWT.Decode<TokenGenerate>(validateTokenRequest.Token, Configuration.SecretKey);
                token.Value = validateTokenRequest.Token;

                if(token != null)
                {
                    if (token.IsValid == true && token.EntityType.ToLower() == validateTokenRequest.EntityType.ToLower() && token.EntityId == validateTokenRequest.EntityId)
                        return true;
                }
                return false;
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}