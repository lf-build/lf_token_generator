﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using RestSharp;


namespace LendFoundry.Token.Generator
{
    public class TokenGeneratorService : ITokenGeneratorService
    {
        private IServiceClient Client { get; }

        public TokenGeneratorService(IServiceClient client)
        {
            Client = client;
        }

        
        public async Task<ITokenGenerate> Issue(ITokenGenerate token)
        {
            var request = new RestRequest("/", Method.POST);        
            request.AddJsonBody(token);
            return await Client.ExecuteAsync<TokenGenerate>(request);
        }
         

        public async Task<bool> ValidateToken(IValidateTokenRequest validateTokenRequest)
        {
            var request = new RestRequest("/validate", Method.POST);
            request.AddJsonBody(validateTokenRequest);
            return await Client.ExecuteAsync<bool>(request);
        }
    }
}
