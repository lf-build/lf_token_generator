﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Token.Generator.Client
{
    public static class TokenGeneratorServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddTokenGeneratorService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ITokenGeneratorServiceFactory>(p => new TokenGeneratorServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITokenGeneratorServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddTokenGeneratorService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<ITokenGeneratorServiceFactory>(p => new TokenGeneratorServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<ITokenGeneratorServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddTokenGeneratorService(this IServiceCollection services)
        {
            services.AddTransient<ITokenGeneratorServiceFactory>(p => new TokenGeneratorServiceFactory(p));
            services.AddTransient(p => p.GetService<ITokenGeneratorServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
