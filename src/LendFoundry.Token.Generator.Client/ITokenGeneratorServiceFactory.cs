﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Token.Generator.Client
{
    public interface ITokenGeneratorServiceFactory
    {
        ITokenGeneratorService Create(ITokenReader reader);
    }
}
