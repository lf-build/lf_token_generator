using System;


namespace LendFoundry.Token.Generator
{
    public interface IValidateTokenRequest
    {
        
        string EntityId { get; set; }

        string EntityType { get; set; }

        string Token { get; set; }
    }
}