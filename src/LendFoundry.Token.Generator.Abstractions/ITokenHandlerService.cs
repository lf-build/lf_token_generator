using System;

namespace LendFoundry.Token.Generator
{
    public interface ITokenHandlerService
    {
        ITokenGenerate Issue(string tenant, string issuer);
        ITokenGenerate Issue(string tenant, string issuer, DateTime? expiration, string subject, string[] scope);
        void Write(ITokenGenerate token);
        ITokenGenerate Parse(string value = null);
    }
}