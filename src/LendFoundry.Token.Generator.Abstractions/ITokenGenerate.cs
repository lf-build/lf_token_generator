using System;


namespace LendFoundry.Token.Generator
{
    public interface ITokenGenerate
    {
        string Issuer { get; }

        DateTime IssuedAt { get; }

        DateTime? Expiration { get; }

        string Subject { get; }

        string Tenant { get; }

        string Value { get; }

        int ExpirationDays { get; }

        bool IsValid { get; }

        string EntityId { get; set; }

        string EntityType { get; set; }
    }
}