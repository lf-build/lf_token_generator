﻿using System;

namespace LendFoundry.Token.Generator
{
    /// <summary>
    /// Settings class
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        /// <value>
        /// The name of the service.
        /// </value>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "token-generator";
    }
}
