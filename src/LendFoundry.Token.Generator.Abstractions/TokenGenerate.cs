using System;


namespace LendFoundry.Token.Generator
{
    public class TokenGenerate : ITokenGenerate
    {

        public string Issuer { get; set; }


        public DateTime IssuedAt { get; set; }


        public DateTime? Expiration { get; set; }


        public string Subject { get; set; }

        public string Tenant { get; set; }


        public int ExpirationDays { get; set; }


        public string EntityType { get; set; }



        public string EntityId { get; set; }


        public string Value { get; set; }

        public bool IsValid =>!string.IsNullOrEmpty(Tenant) && (!Expiration.HasValue || Expiration > DateTime.UtcNow);
    }
}