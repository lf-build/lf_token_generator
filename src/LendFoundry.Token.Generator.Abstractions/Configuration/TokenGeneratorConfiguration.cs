using System.Collections.Generic;

namespace LendFoundry.Token.Generator.Configuration
{
    public class TokenGeneratorConfiguration : ITokenGeneratorConfiguration
    {
        public int ExpirationDays { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}