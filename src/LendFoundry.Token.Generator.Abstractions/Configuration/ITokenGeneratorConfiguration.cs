﻿using LendFoundry.Foundation.Client;

namespace LendFoundry.Token.Generator.Configuration
{
    public interface ITokenGeneratorConfiguration : IDependencyConfiguration
    {
      int ExpirationDays { get; set; }
    }
}
