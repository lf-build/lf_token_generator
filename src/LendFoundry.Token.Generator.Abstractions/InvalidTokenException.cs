﻿using System;

namespace LendFoundry.Token.Generator
{
    public class InvalidTokenException : Exception
    {
        public InvalidTokenException(string message) : base(message)
        {

        }
    }
}
