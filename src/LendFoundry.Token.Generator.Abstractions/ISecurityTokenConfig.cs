namespace LendFoundry.Token.Generator
{
    public interface ITokenConfig
    {
        byte[] SecretKey { get; }

    }
}