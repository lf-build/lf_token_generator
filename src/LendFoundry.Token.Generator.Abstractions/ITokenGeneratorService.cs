using LendFoundry.Token.Generator;
using System;
using System.Threading.Tasks;

namespace LendFoundry.Token.Generator
{
    public interface ITokenGeneratorService
    {

        Task<ITokenGenerate> Issue(ITokenGenerate token);       
        Task<bool> ValidateToken(IValidateTokenRequest validateTokenRequest);
    }
}